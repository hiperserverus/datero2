export class Animal {

    constructor(
         public _id: String,
         public name: String,
         public number: String,
         public image: String
    ) {}
}
