export class Play {

    constructor(

        public _id: String,
        public hora: String,
        public fecha: String,
        public status: String,
        public ticket: String,
        public animal: String
    ) {}
}
