export class Result {

    constructor(
        public _id: string,
        public hora: String,
        public fecha: String,
        public status: String,
        public animal: String
    ) {}
}
