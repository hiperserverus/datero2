export class Ticket {

    constructor(
        public _id: String,
        public serial: String,
        public hora: String,
        public fecha: String,
        public status: String,
        public user: String,
    ) {}
}
